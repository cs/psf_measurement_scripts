import requests

class PicoControlClient:
	
	def __init__(self, server_address):
		self.server_address = server_address
		
	def move(self, picomotor, distance):
		response  = requests.post(self.server_address + '/move', json=dict(board=picomotor[0], channel = picomotor[1], distance=distance))
		return response.status_code
		
	def stop(self):
		response  = requests.post(self.server_address + '/stop')
		return response.status_code
		
	def get_status(self):
		response = requests.get(self.server_address + "/status").json()
		return response 
		
	def get_config(self):
		response = requests.get(self.server_address + "/config").json()
		return response 
		
	def set_speed(self, new_speed):
		response  = requests.post(self.server_address + '/config', json=dict(velocity=new_speed))
		return response.status_code
		
		
		
