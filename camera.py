import dataclasses
from dataclasses import dataclass
import numpy as np
from typing import Any, Dict, List, Optional, Tuple, TYPE_CHECKING

from qcontrol3.driver.camera.andor2.andor import (
    AcquisitionMode,
    Andor,
    CtCapabilities,
    OutputAmplificationMode,
    ReadMode,
    TriggerMode,
    ShutterMode,
    FanMode,
    TTLMode,
    CoolerMode,
    EMGainMode,
)

if TYPE_CHECKING:
    from nptyping import NDArray


@dataclass
class ADChannelInfo:
    bit_depth: int
    hs_speeds: Dict[OutputAmplificationMode, List[float]]


@dataclass
class DeviceInfo:
    """container for the device specific, but constant information"""

    ad_channels: List[ADChannelInfo]
    capabilities: CtCapabilities
    head_name: str
    minimum_image_length: int
    pixel_count: Tuple[int, int]
    pixel_size: Tuple[float, float]
    pre_amp_gains: List[float]
    serial_number: int
    temperature_range: Tuple[int, int]
    vs_speeds: List[float]


@dataclass
class ImageParameters:
    hbin: int
    hstart: int
    hend: int
    vbin: int
    vstart: int
    vend: int

    @property
    def width(self) -> int:
        return self.hend - self.hstart + 1

    @property
    def height(self) -> int:
        return self.vend - self.vstart + 1


@dataclass
class ShutterParameters:
    typ: TTLMode
    mode: ShutterMode
    closingtime: int
    openingtime: int


class AndorCamera:
    """convenient wrapper around the andor driver

    the purpose of this class is to wrap the bare andor driver into something
    which is more user friendly. This class also caches all the values which
    can only be set, but not read.

    """

    def __init__(self, serial: Optional[int] = None):
        if serial is not None:
            self.driver = Andor.from_serial(serial)
        else:
            self.driver = Andor()

        self._device_info = self._query_device_info()

        self._acquisition_mode: AcquisitionMode = None
        self._frame_transfer_mode: bool = None
        self._image_parameters = ImageParameters(
            hbin=1,
            vbin=1,
            hstart=1,
            vstart=1,
            hend=self._device_info.pixel_count[0],
            vend=self._device_info.pixel_count[1],
        )
        self._shutter_parameters = ShutterParameters(
            typ=TTLMode.ActiveHigh, mode=ShutterMode.Auto, closingtime=0, openingtime=0
        )
        self._number_accumulations: int = None
        self._number_kinetics: int = None
        self._read_mode: ReadMode = None
        self._fan_mode: FanMode = None
        self._trigger_mode: TriggerMode = None
        self._cooler_on: bool = self.driver.IsCoolerOn()
        self._cooler_mode: CoolerMode = None
        self._pre_amp_gain_index: int = None
        self._em_gain_mode = None

    def _query_device_info(self) -> DeviceInfo:
        """read the constant camera information from the device"""
        return DeviceInfo(
            ad_channels=self._query_ad_channel_info(),
            capabilities=self.driver.GetCapabilities(),
            head_name=self.driver.GetHeadModel(),
            minimum_image_length=self.driver.GetMinimumImageLength(),
            pixel_count=self.driver.GetDetector(),
            pixel_size=self.driver.GetPixelSize(),
            serial_number=self.driver.GetCameraSerialNumber(),
            temperature_range=self.driver.GetTemperatureRange(),
            pre_amp_gains=[
                self.driver.GetPreAmpGain(index)
                for index in range(self.driver.GetNumberPreAmpGains())
            ],
            vs_speeds=[
                self.driver.GetVSSpeed(index)
                for index in range(self.driver.GetNumberVSSpeeds())
            ],
        )

    def _query_ad_channel_info(self) -> List[ADChannelInfo]:
        channel_count = self.driver.GetNumberADChannels()
        return [
            ADChannelInfo(
                bit_depth=self.driver.GetBitDepth(channel),
                hs_speeds={
                    typ: [
                        self.driver.GetHSSpeed(channel, typ, index)
                        for index in range(self.driver.GetNumberHSSpeeds(channel, typ))
                    ]
                    for typ in (OutputAmplificationMode.EM,)
                },
            )
            for channel in range(channel_count)
        ]

    @property
    def device_info(self) -> DeviceInfo:
        return self._device_info

    @property
    def acquisition_mode(self) -> AcquisitionMode:
        return self._acquisition_mode

    @acquisition_mode.setter
    def acquisition_mode(self, value: AcquisitionMode) -> None:
        if self._acquisition_mode == value:
            return
        self.driver.SetAcquisitionMode(value)
        self._acquisition_mode = value

    @property
    def binning_horizontal(self):
        return self._image_parameters.hbin

    @binning_horizontal.setter
    def binning_horizontal(self, value):
        if self._image_parameters.hbin == value:
            return
        self.update_image_parameters(vbin=value)

    @property
    def binning_vertical(self):
        return self._image_parameters.vbin

    @binning_vertical.setter
    def binning_vertical(self, value):
        if self._image_parameters.vbin == value:
            return
        self.update_image_parameters(vbin=value)

    @property
    def cooler_status(self) -> bool:
        return self.driver.IsCoolerOn()

    @cooler_status.setter
    def cooler_status(self, value: bool) -> None:
        if value == self.cooler_status:
            return
        if value is True:
            self.driver.CoolerON()
        else:
            self.driver.CoolerOFF()

    @property
    def cooler_mode(self) -> CoolerMode:
        return self._cooler_mode

    @cooler_mode.setter
    def cooler_mode(self, value: CoolerMode):
        if value == self._cooler_mode:
            return
        self.driver.SetCoolerMode(value)
        self._cooler_mode = value

    @property
    def em_gain_mode(self) -> EMGainMode:
        return self._em_gain_mode

    @em_gain_mode.setter
    def em_gain_mode(self, value: EMGainMode):
        if value == self._em_gain_mode:
            return
        self.driver.SetEMGainMode(value)
        self._em_gain_mode = value

    @property
    def em_gain(self) -> int:
        return self.driver.GetEMCCDGain()

    @em_gain.setter
    def em_gain(self, value: int):
        self.driver.SetEMCCDGain(value)

    @property
    def exposure_time(self) -> float:
        return self.driver.GetAcquisitionTimings().exposure

    @exposure_time.setter
    def exposure_time(self, value: float) -> None:
        self.driver.SetExposureTime(value)

    @property
    def fan_mode(self) -> FanMode:
        return self._fan_mode

    @fan_mode.setter
    def fan_mode(self, value: FanMode) -> None:
        if value == self._fan_mode:
            return
        self.driver.SetFanMode(value)
        self._fan_mode = value

    @property
    def frame_transfer_mode(self) -> bool:
        return self._frame_transfer_mode

    @frame_transfer_mode.setter
    def frame_transfer_mode(self, value):
        if self._frame_transfer_mode == value:
            return
        self.driver.SetFrameTransferMode(value)
        self._frame_transfer_mode = value

    @property
    def image_parameters(self) -> ImageParameters:
        return self._image_parameters

    @image_parameters.setter
    def image_parameters(self, value: ImageParameters) -> None:
        if value == self._image_parameters:
            return
        self.driver.SetImage(**dataclasses.asdict(value))
        self._image_parameters = value

    @property
    def number_accumulations(self) -> int:
        return self._number_accumulations

    @number_accumulations.setter
    def number_accumulations(self, value: int) -> None:
        if value == self._number_accumulations:
            return
        self.driver.SetNumberAccumulations(value)
        self._number_accumulations = value

    @property
    def number_kinetics(self) -> int:
        return self._number_kinetics

    @number_kinetics.setter
    def number_kinetics(self, value: int) -> None:

        if value == self._number_kinetics:
            return
        self.driver.SetNumberKinetics(value)
        self._number_kinetics = value

    @property
    def pre_amp_gain_index(self):
        return self._pre_amp_gain_index

    @pre_amp_gain_index.setter
    def pre_amp_gain_index(self, value: int):
        if self._pre_amp_gain_index == value:
            return
        self.driver.SetPreAmpGain(value)
        self._pre_amp_gain_index = value

    @property
    def read_mode(self) -> ReadMode:
        return self._read_mode

    @read_mode.setter
    def read_mode(self, value: ReadMode) -> None:
        if value == self._read_mode:
            return
        self.driver.SetReadMode(value)
        self._read_mode = value

    @property
    def shutter_parameters(self) -> ShutterParameters:
        return self._shutter_parameters

    @shutter_parameters.setter
    def shutter_parameters(self, value: ShutterParameters):
        if value == self._shutter_parameters:
            return
        self.driver.SetShutter(**dataclasses.asdict(value))
        self._shutter_parameters = ShutterParameters

    @property
    def temperature(self) -> int:
        t_value, t_status = self.driver.GetTemperature()
        return t_value

    @temperature.setter
    def temperature(self, value: int):
        self.driver.SetTemperature(value)

    @property
    def trigger_mode(self) -> TriggerMode:
        return self._trigger_mode

    @trigger_mode.setter
    def trigger_mode(self, value: TriggerMode) -> None:
        if self._trigger_mode == value:
            return
        self.driver.SetTriggerMode(value)
        self._trigger_mode = value

    def get_acquired_data(self) -> "NDArray[(Any, ...), np.uint16]":
        """loads the latest acquired image from the camera

        when using single mode"""
        image_shape = self._get_image_shape()
        data = self.driver.GetAcquiredData16(np.multiply.reduce(image_shape))
        # for some reason 1000 is the lowest value
        data -= 1000
        return data.reshape(image_shape)

    def _get_image_shape(self) -> Tuple[int, ...]:
        """calculate the number of pixels per read quantity

        the number of pixels is returned as a shape tuple, usable to create
        an appropriately sized numpy array. The number of dimensions depends on
        the imaging mode used
        """
        ip = self._image_parameters
        if self._read_mode == ReadMode.Image:
            if self._acquisition_mode == AcquisitionMode.SingleScan:
                return (
                    ip.height // ip.vbin,
                    ip.width // ip.hbin,
                )
            elif (
                self._acquisition_mode == AcquisitionMode.Kinetics
                or self._acquisition_mode == AcquisitionMode.FastKinetics
            ):
                return (
                    self._number_kinetics,
                    ip.height // ip.vbin,
                    ip.width // ip.hbin,
                )

            else:
                raise NotImplementedError
        elif (
            self._read_mode == ReadMode.SingleTrack
            or self._read_mode == ReadMode.FullVerticalBinning
        ):
            if self._acquisition_mode == AcquisitionMode.SingleScan:
                return (ip.width,)
            elif self._acquisition_mode == AcquisitionMode.Kinetics:
                return self._number_kinetics, ip.width
            else:
                raise NotImplementedError
        else:
            raise NotImplementedError

    def update_image_parameters(self, **kwargs):
        params = dataclasses.asdict(self._image_parameters)
        params.update(kwargs)
        self.driver.SetImage(**params)
        self._image_parameters = ImageParameters(**params)
