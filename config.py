config_dict = {
	"picomotors": { #(module, channel)
		(1,1): {"name": "obj_top_x", "direction": 1},
		(1,2): {"name": "obj_top_y", "direction": 1},
		(1,3): {"name": "obj_top_z1", "direction": 1},
		(1,4): {"name": "obj_top_z2", "direction": 1},
		(1,6): {"name": "obj_top_z3", "direction": 1},
	},
	"modes": {
		1: ["Default Mode"],
		2: ["Z Translation", [(1,3), (1,4), (1,6)]],
		3: ["Tilt 12", [(1,4), (1,5)]],
		4: ["Tilt 4", [(1,5), (1,3)]],
		5: ["Tilt 8", [(1,3), (1,4)]],
	},
	"motor_settings": {
		"speed": 200,
		"acceleration": 100000,
		"steps": 500,
	}
}
