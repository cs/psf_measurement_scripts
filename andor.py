#   pyAndor - A Python wrapper for Andor's scientific cameras
#   Copyright (C) 2009  Hamid Ohadi
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.


import ctypes as ct
from dataclasses import dataclass
import enum
import logging
import numpy as np
import platform
import sys
import time
from typing import (
    Any,
    Callable,
    List,
    NewType,
    Optional,
    Set,
    Tuple,
    TYPE_CHECKING,
)
import warnings


if TYPE_CHECKING:
    from nptyping import NDArray

    if sys.platform == "win32":
        from win32 import PyHANDLE

"""Andor class which is meant to provide the Python version of the same
   functions that are defined in the Andor's SDK. Since Python does not
   have pass by reference for immutable variables, some of these variables
   are actually stored in the class instance. For example the temperature,
   gain, gainRange, status etc. are stored in the class. """


class ErrorCode(enum.IntEnum):
    DRV_ERROR_CODES = 20001
    DRV_SUCCESS = 20002
    DRV_VXDNOTINSTALLED = 20003
    DRV_ERROR_SCAN = 20004
    DRV_ERROR_CHECK_SUM = 20005
    DRV_ERROR_FILELOAD = 20006
    DRV_UNKNOWN_FUNCTION = 20007
    DRV_ERROR_VXD_INIT = 20008
    DRV_ERROR_ADDRESS = 20009
    DRV_ERROR_PAGELOCK = 20010
    DRV_ERROR_PAGEUNLOCK = 20011
    DRV_ERROR_BOARDTEST = 20012
    DRV_ERROR_ACK = 20013
    DRV_ERROR_UP_FIFO = 20014
    DRV_ERROR_PATTERN = 20015

    DRV_ACQUISITION_ERRORS = 20017
    DRV_ACQ_BUFFER = 20018
    DRV_ACQ_DOWNFIFO_FULL = 20019
    DRV_PROC_UNKONWN_INSTRUCTION = 20020
    DRV_ILLEGAL_OP_CODE = 20021
    DRV_KINETIC_TIME_NOT_MET = 20022
    DRV_ACCUM_TIME_NOT_MET = 20023
    DRV_NO_NEW_DATA = 20024
    DRV_PCI_DMA_FAIL = 20025
    DRV_SPOOLERROR = 20026
    DRV_SPOOLSETUPERROR = 20027
    DRV_FILESIZELIMITERROR = 20028
    DRV_ERROR_FILESAVE = 20029

    DRV_TEMPERATURE_CODES = 20033
    DRV_TEMPERATURE_OFF = 20034
    DRV_TEMPERATURE_NOT_STABILIZED = 20035
    DRV_TEMPERATURE_STABILIZED = 20036
    DRV_TEMPERATURE_NOT_REACHED = 20037
    DRV_TEMPERATURE_OUT_RANGE = 20038
    DRV_TEMPERATURE_NOT_SUPPORTED = 20039
    DRV_TEMPERATURE_DRIFT = 20040

    DRV_TEMP_CODES = 20033
    DRV_TEMP_OFF = 20034
    DRV_TEMP_NOT_STABILIZED = 20035
    DRV_TEMP_STABILIZED = 20036
    DRV_TEMP_NOT_REACHED = 20037
    DRV_TEMP_OUT_RANGE = 20038
    DRV_TEMP_NOT_SUPPORTED = 20039
    DRV_TEMP_DRIFT = 20040

    DRV_GENERAL_ERRORS = 20049
    DRV_INVALID_AUX = 20050
    DRV_COF_NOTLOADED = 20051
    DRV_FPGAPROG = 20052
    DRV_FLEXERROR = 20053
    DRV_GPIBERROR = 20054
    DRV_EEPROMVERSIONERROR = 20055

    DRV_DATATYPE = 20064
    DRV_DRIVER_ERRORS = 20065
    DRV_P1INVALID = 20066
    DRV_P2INVALID = 20067
    DRV_P3INVALID = 20068
    DRV_P4INVALID = 20069
    DRV_INIERROR = 20070
    DRV_COFERROR = 20071
    DRV_ACQUIRING = 20072
    DRV_IDLE = 20073
    DRV_TEMPCYCLE = 20074
    DRV_NOT_INITIALIZED = 20075
    DRV_P5INVALID = 20076
    DRV_P6INVALID = 20077
    DRV_INVALID_MODE = 20078
    DRV_INVALID_FILTER = 20079

    DRV_I2CERRORS = 20080
    DRV_I2CDEVNOTFOUND = 20081
    DRV_I2CTIMEOUT = 20082
    DRV_P7INVALID = 20083
    DRV_USBERROR = 20089
    DRV_IOCERROR = 20090
    DRV_VRMVERSIONERROR = 20091
    DRV_USB_INTERRUPT_ENDPOINT_ERROR = 20093
    DRV_RANDOM_TRACK_ERROR = 20094
    DRV_INVALID_TRIGGER_MODE = 20095
    DRV_LOAD_FIRMWARE_ERROR = 20096
    DRV_DIVIDE_BY_ZERO_ERROR = 20097
    DRV_INVALID_RINGEXPOSURES = 20098

    DRV_ERROR_NOCAMERA = 20990
    DRV_NOT_SUPPORTED = 20991
    DRV_NOT_AVAILABLE = 20992

    DRV_ERROR_MAP = 20115
    DRV_ERROR_UNMAP = 20116
    DRV_ERROR_MDL = 20117
    DRV_ERROR_UNMDL = 20118
    DRV_ERROR_BUFFSIZE = 20119
    DRV_ERROR_NOHANDLE = 20121

    DRV_GATING_NOT_AVAILABLE = 20130
    DRV_FPGA_VOLTAGE_ERROR = 20131

    DRV_MSTIMINGS_ERROR = 20156


class AndorException(Exception):
    def __init__(self, error_code: int, message):
        self.error_code = error_code
        try:
            msg = f"{ErrorCode(error_code).name} ({error_code}): {message}"
        except ValueError:
            # error_code not in ErrorCode
            msg = f"Unknown Error ({error_code}): {message}"
        super().__init__(msg)


class AcquisitionMode(enum.IntEnum):
    SingleScan = 1
    Accumulate = 2
    Kinetics = 3
    FastKinetics = 4
    RunTillAbort = 5


class CoolerMode(enum.IntEnum):
    MaintainAfterShutdown = 1
    AmbientAfterShutdown = 0


@dataclass
class AcquisitionTimings:
    exposure: float
    accumulate: float
    kinetic: float


class EMGainMode(enum.IntEnum):
    DAC8 = 0
    DAC12 = 1
    Linear = 2
    Real = 3


class FanMode(enum.IntEnum):
    Full = 0
    Low = 1
    Off = 2


class OutputAmplificationMode(enum.IntEnum):
    EM = 0
    Conventional = 1


class ReadMode(enum.IntEnum):
    FullVerticalBinning = 0
    MultiTrack = 1
    RandomTrack = 2
    SingleTrack = 3
    Image = 4


class ShutterMode(enum.IntEnum):
    Auto = 0
    Open = 1
    Close = 2
    OpenForFVB = 4
    OpenForAny = 5


class Status(enum.IntEnum):
    Idle = ErrorCode.DRV_IDLE
    TempCycle = ErrorCode.DRV_TEMPCYCLE
    Acquiring = ErrorCode.DRV_ACQUIRING
    AccumTimeNotMet = ErrorCode.DRV_ACCUM_TIME_NOT_MET
    KineticTimeNotMet = ErrorCode.DRV_KINETIC_TIME_NOT_MET
    ErrorAck = ErrorCode.DRV_ERROR_ACK
    AcqBuffer = ErrorCode.DRV_ACQ_BUFFER
    SpoolError = ErrorCode.DRV_SPOOLERROR


class TemperatureStatus(enum.IntEnum):
    Off = (ErrorCode.DRV_TEMP_OFF,)
    NotStabilized = (ErrorCode.DRV_TEMP_NOT_STABILIZED,)
    Stabilized = (ErrorCode.DRV_TEMP_STABILIZED,)
    NotReached = (ErrorCode.DRV_TEMP_NOT_REACHED,)
    OutRange = (ErrorCode.DRV_TEMP_OUT_RANGE,)
    Drift = (ErrorCode.DRV_TEMP_DRIFT,)


class TriggerMode(enum.IntEnum):
    Internal = 0
    External = 1
    ExternalStart = 6
    ExternalExposure = 7
    ExternalFVBEM = 9
    Software = 10
    ExternalChargeShifting = 12


class TTLMode(enum.IntEnum):
    ActiveLow = 0
    ActiveHigh = 1


def find_dll() -> ct.CDLL:
    # Check operating system and load library
    # for Windows
    if platform.system() == "Windows":
        if platform.architecture()[0] == "32bit":
            return ct.cdll.LoadLibrary(
                "C:\\Program Files\\Andor SOLIS\\Drivers\\atmcd32d"
            )
        else:
            return ct.cdll.LoadLibrary("C:\\Program Files\\Andor SDK\\atmcd64d")
    # for Linux
    elif platform.system() == "Linux":
        dllname = "/usr/local/lib/libandor.so"
        return ct.cdll.LoadLibrary(dllname)
    else:
        raise Exception("Can not find Andor DLL")


def _add_typing(dll):
    """add ctypes typing information to some functions in the library

    currently only done to allow to pass numpy arrays as buffers to c functions.
    See https://numpy.org/doc/stable/reference/routines.ctypeslib.html#numpy.ctypeslib.ndpointer
    """
    argtype_info = dict(
        GetAcquiredData=[
            np.ctypeslib.ndpointer(np.int32, ndim=1, flags="C_CONTIGUOUS"),
            ct.c_ulong,
        ],
        GetAcquiredData16=[
            np.ctypeslib.ndpointer(np.uint16, ndim=1, flags="C_CONTIGUOUS"),
            ct.c_ulong,
        ],
    )

    for fun_name, argtypes in argtype_info.items():
        fun = getattr(dll, fun_name)
        fun.argtypes = argtypes
        fun.restype = ct.c_uint


CameraHandle = NewType("CameraHandle", int)


class CtCapabilities(ct.Structure):
    _fields_ = [
        ("Size", ct.c_uint),
        ("AcqModes", ct.c_uint),
        ("ReadModes", ct.c_uint),
        ("TriggerModes", ct.c_uint),
        ("CameraType", ct.c_uint),
        ("PixelMode", ct.c_uint),
        ("SetFunctions", ct.c_uint),
        ("GetFunctions", ct.c_uint),
        ("Features", ct.c_uint),
        ("PCICard", ct.c_uint),
        ("EMGainCapability", ct.c_uint),
        ("FTReadModes", ct.c_uint),
    ]


class Andor:
    """ctypes wrapper around the Andor SDK2 library.

    This class does nothing more than exposing the functions as they are provided
    by the library. Main feature is the automatic checking of return codes and
    the raising of exceptions where necessary. Also enum classes are provided
    where they are applicable.
    """

    _dll: ct.CDLL = None

    def __init__(
        self,
        logger: Optional[logging.Logger] = None,
        camera_handle: Optional[int] = None,
    ):
        self._logger: Optional[logging.Logger] = logger
        self._handle: Optional[int] = camera_handle

        self.Initialize()

        return

        # Get the speeds for the shifts
        self.GetNumberVSSpeeds()
        self.GetVSSpeed()
        self.GetNumberHSSpeeds()
        self.GetHSSpeed()
        self.GetNumberVSAmplitudes()
        # Temperature
        self.GetTemperatureRange()
        self.SetTemperature(self.setTemp)
        self.CoolerON()
        self.SetCoolerMode(1)
        self.SetFanMode(2)

        self.SetReadMode(4)  # 4: Full picture
        self.SetShutter(1, 1, 0, 0)
        # 1st parameter: 0: low=open, 1: low=closed ;
        # 2nd parameter: 1 = perm. open, 2 = perm. closed ;
        # 3rd and 4th parameters: time to open/close
        self.GetDetector()
        self.SetImage(1, 1, 1, self.width, 1, self.height)
        self.GetNumberADChannels()
        self.GetNumberPreAmpGains()
        self.SetOutputAmplifier(0)  # ixon only has EM
        self.GetPreAmpGain()
        self.GetBitDepth()
        self.SetPreAmpGain(0)
        self.SetEMGainMode(1)  # values set in range 0-4095
        self.GetEMGainRange()
        self.GetEMCCDGain()
        #### fast initialization
        #        self.SetVSSpeed(0) #initialize to fastest speeds
        #        self.SetHSSpeed(self.outamp, 0) #initialize to fastest speeds
        #### slow initialization
        self.SetVSSpeed(self.noVSSpeeds - 1)  # initialize to slowest speeds
        self.SetHSSpeed(
            self.outamp, self.noHSSpeeds - 1
        )  # initialize to slowest speeds

    #        self.status      = self.__ERROR_CODE[error]

    def __del__(self):
        """
        gets called by the garbage collector and shuts the camera down properly
        """
        if self._handle is not None:
            self.dll.SetCameraCamera(ct.c_long(self._handle))
        self.dll.ShutDown()

    @classmethod
    def from_serial(cls, serial: int, **kwargs):
        """creates a camera object for the given serial number"""

        all_cameras = cls.list_cameras()
        for handle in all_cameras:
            camera = cls(camera_handle=handle, **kwargs)
            if camera.device_info.serial_number == serial:
                return camera

        raise Exception(f"Could not find camera with serial={serial}")

    @classmethod
    def list_cameras(cls) -> List[CameraHandle]:
        dll = cls.get_dll()
        totalCameras = ct.c_long()
        error = dll.GetAvailableCameras(ct.byref(totalCameras)).value
        if error != ErrorCode.DRV_SUCCESS:
            raise AndorException(error, "could not get number of cameras")
        handles: List[CameraHandle] = []
        for i in range(totalCameras.value):
            handle = ct.c_long()
            error = dll.GetCameraHandle(ct.c_long(i), ct.byref(handle)).value
            if error != ErrorCode.DRV_SUCCESS:
                raise AndorException(error, "could not get camera handle")
            handles.append(handle.value)
        return handles

    @classmethod
    def get_dll(cls):
        if cls._dll is None:
            cls._dll = find_dll()
            _add_typing(cls._dll)
        return cls._dll

    @property
    def dll(self):
        """return the dll

        always returns the same dll handle"""
        return self.get_dll()

    def _call(
        self,
        function: Callable[[Tuple[Any]], ct.c_uint],
        *args,
        errors_ok: Set[ErrorCode] = set(),
    ) -> ErrorCode:
        """calls the specified function in the andor dll, checking errors

        this function should be used for all calls to Andor driver functions.
        It will call the function and check for errors, raising an exception
        """
        if self._handle:
            error = self.dll.SetCurrentCamera(ct.c_long(self._handle)).value
            if error != ErrorCode.DRV_SUCCESS:
                raise AndorException(error, "failed to set handle")

        error = function(*args)
        if error != ErrorCode.DRV_SUCCESS and error not in errors_ok:
            raise AndorException(error, "")
        if self._logger is not None:
            self._logger.debug(
                f"Andor: {function.__name__} returned {ErrorCode(error).name} ({error})"
            )
        return error

    def AbortAcquisition(self) -> ErrorCode:
        """This function aborts the current acquisition if one is active."""
        return self._call(self.dll.AbortAcquisition, errors_ok={ErrorCode.DRV_IDLE})

    def Initialize(self) -> None:
        """
        This function will initialize the Andor SDK System. As part of the
        initialization procedure on some cameras (i.e. Classic, iStar and
        earlier iXion) the DLL willneed access to a DETECTOR.INI which
        contains  information  relating  to the  detector  head,  number
        pixels, readout speeds etc. If your system has multiple cameras
        then see the section Controlling multiple cameras
        """
        tekst = ct.c_char()
        self._call(self.dll.Initialize, ct.byref(tekst))

    def ShutDown(self) -> None:
        """
        This function will close the AndorMCD system down.
        """
        self._call(self.dll.ShutDown)

    def GetCameraSerialNumber(self) -> int:
        """
        This function will retrieve camera’s serial number.
        """
        serial = ct.c_int()
        self._call(self.dll.GetCameraSerialNumber, ct.byref(serial))
        return serial.value

    def GetDetector(self) -> Tuple[int, int]:
        """
        This  function  returns  the  size  of  the  detector  in  pixels.
        The  horizontal  axis  is  taken  to  be the axis parallel to the
        readout register.
        """

        cw = ct.c_int()
        ch = ct.c_int()

        self._call(self.dll.GetDetector, ct.byref(cw), ct.byref(ch))
        return cw.value, ch.value

    def GetHeadModel(self) -> str:
        name = ct.create_string_buffer(1024)
        self._call(self.dll.GetHeadModel, ct.pointer(name))
        return name.value

    def GetMinimumImageLength(self) -> int:
        """
        This function will return the minimum number of pixels that can be
        read out from the chip at each exposure. This minimum value arises
        due the way in which the chip is read out and will limit the possible
        sub image dimensions and binning sizes that can be applied.
        """
        minImageLength = ct.c_int()
        self._call(self.dll.GetMinimumImageLength, ct.byref(minImageLength))
        return minImageLength.value

    def GetPixelSize(self) -> Tuple[float, float]:
        xSize = ct.c_float()
        ySize = ct.c_float()
        self._call(self.dll.GetPixelSize, ct.byref(xSize), ct.byref(ySize))
        return xSize.value, ySize.value

    def SetReadMode(self, mode: ReadMode) -> None:
        """This function will set the readout mode to be used on
        the subsequent acquisitions.

        Parameters
            int mode: readout mode
            Valid values:
                0 Full Vertical Binning
                1 Multi-Track
                2 Random-Track
                3 Single-Track
                4 Image
        """
        self._call(self.dll.SetReadMode, ct.c_int(mode))

    def SetAcquisitionMode(self, mode: AcquisitionMode) -> None:
        """
        This function will set the acquisition mode to be used on the next
        StartAcquisition.
        Parameters
            int mode: the acquisition mode.
            Valid values:
            1 Single Scan
            2 Accumulate
            3 Kinetics
            4 Fast Kinetics
            5 Run till abort
        """
        self._call(self.dll.SetAcquisitionMode, ct.c_int(mode))

    def SetFastKinetics(
        self, expRows, serLength, expTime, binning=None, hbin=None, vbin=None
    ):
        """
        This function will set the parameters to be used
        when taking a fast kinetics acquisition.

        Parameters
            int exposedRows: sub-area height in rows.
            int seriesLength: number in series.
            float time: exposure time in seconds.
            int mode: binning mode (0 FVB, 4 Image). -> python defaults to 4
            int hbin: horizontal binning. -> python defaults to 1
            int vbin: vertical binning (only used when in image mode). -> python defaults to 1

        Returns (python):
            ERROR_CODE[error]
        """
        if binning == None:
            binning = 4
        if hbin == None:
            hbin = 1
        if vbin == None:
            vbin = 1

        error = self.dll.SetFastKinetics(
            expRows, serLength, ct.c_float(expTime), binning, hbin, vbin
        )
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.scans = self.height // expRows
            self.expRows = expRows
            self.vbin = vbin
            self.hbin = hbin
        return self.__ERROR_CODE[error]

    def SetFastKineticsEx(
        self,
        expRows,
        serLength,
        expTime,
        binning=None,
        hbin=None,
        vbin=None,
        offset=None,
    ):
        """
        This function is the same as SetFastKinetics with
        the addition of an Offset parameter, which will
        inform the SDK of the first row to be used.

        Parameters
            int exposedRows: sub-area height in rows.
            int seriesLength: number in series.
            float time: exposure time in seconds.
            int mode: binning mode (0 – FVB , 4 – Image).
            int hbin: horizontal binning.
            int vbin: vertical binning (only used when in image mode).
            int offset: offset of first row to be used in Fast Kinetics from the bottom of the CCD

        Returns (python):
            ERROR_CODE[error]
        """
        if binning == None:
            binning = 4
        if hbin == None:
            hbin = 1
        if vbin == None:
            vbin = 1
        if offset == None:
            offset = 0
        error = self.dll.SetFastKinetics(
            expRows, serLength, ct.c_float(expTime), binning, hbin, vbin, offset
        )
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.scans = self.height // expRows
            self.expRows = expRows
            self.offsetRows = offset
            self.vbin = vbin
            self.hbin = hbin
        return self.__ERROR_CODE[error]

    def SetNumberKinetics(self, numKin: int) -> None:
        """
        This  function  will  set  the  number  of  scans
        (possibly  accumulated  scans)  to  be  taken during
        a single acquisition sequence. This will only take effect
        if the acquisition mode is Kinetic Series.

        Parameters
            int number: number of scans to store
        """
        self._call(self.dll.SetNumberKinetics, ct.c_int(numKin))

    def SetNumberAccumulations(self, number: int) -> None:
        """
        This  function  will  set  the  number  of  scans  accumulated
        in  memory.  This  will  only  take effect if the acquisition
        mode is either Accumulate or Kinetic Series.

        Parameters
            int number: number of scans to accumulate
        """
        self._call(self.dll.SetNumberAccumulations, ct.c_int(number))

    def SetAccumulationCycleTime(self, time: float) -> None:
        """
        This function will set the accumulation cycle time to the nearest
        valid value not less than the given value. The actual cycle time
        used is obtained by GetAcquisitionTimings. Please refer to
        SECTION 5 –ACQUISITION MODESfor further information

        Parameters
            float time: the accumulation cycle time in seconds
        """
        self._call(self.dll.SetAccumulationCycleTime, ct.c_float(time))

    def SetKineticCycleTime(self, time: float) -> None:
        """
        This  function  will  set  the  kinetic  cycle  time  to  the
        nearest  valid  value  not  less  than  the given value. The
        actual time used is obtained by GetAcquisitionTimings.
        Please refer to SECTION 5 –ACQUISITION MODESfor further
        information.

        Parameters
            float time: the kinetic cycle time in seconds

        Returns (python):
            ERROR_CODE[error]
        """
        self._call(self.dll.SetKineticCycleTime, ct.c_float(time))

    def SetShutter(
        self, typ: TTLMode, mode: ShutterMode, closingtime: int, openingtime: int
    ) -> None:
        """
        This function controls the behaviour of the shutter.The
        typparameter allows the user to control the TTL signal output
        to an external shutter. The mode parameter configures whether
        the shutter opens & closes automatically (controlled by the camera)
        or is permanently open or permanently closed. The opening and
        closing time specify the time required to open and close the
        shutter (this information is required for calculating acquisition
        timings –see SHUTTER TRANSFER TIME).

        Parameters
            int typ:
                0 Output TTL low signal to open shutter
                1 Output TTL high signal to open shutter
            int mode:
                0 Fully Auto
                1 Permanently Open
                2 Permanently Closed
                4 Openfor FVBseries
                5 Open for anyseries
            int closingtime: Time shutter takes to close (milliseconds)
            int openingtime: Time shutter takes to open (milliseconds)
        """
        self._call(
            self.dll.SetShutter,
            ct.c_int(typ),
            ct.c_int(mode),
            ct.c_int(closingtime),
            ct.c_int(openingtime),
        )

    def SetImage(
        self, hbin: int, vbin: int, hstart: int, hend: int, vstart: int, vend: int
    ) -> None:
        """
        This  function  will  set  the  horizontal  and  vertical
        binning  to  be  used  when  taking  a  full resolution
        image.

        Parameters
            int hbin: number of pixels to bin horizontally.
            int vbin: number of pixels to bin vertically.
            int hstart: Start column (inclusive).
            int hend: End column (inclusive).
            int vstart: Start row (inclusive).
            int vend: End row (inclusive).
        """
        self._call(
            self.dll.SetImage,
            ct.c_int(hbin),
            ct.c_int(vbin),
            ct.c_int(hstart),
            ct.c_int(hend),
            ct.c_int(vstart),
            ct.c_int(vend),
        )

    def StartAcquisition(self) -> None:
        """
        This  function  starts  an  acquisition.
        The  status  of  the  acquisition  can  be  monitored
        via GetStatus().
        """
        self._call(self.dll.StartAcquisition)

    def WaitForAcquisition(self) -> None:
        """
        WaitForAcquisition can be called after an acquisition is started using
        StartAcquisition to put the calling thread to sleep until an
        Acquisition Event occurs. This can be used as a simple alternative to
        the functionality provided by the SetDriverEvent function, as all Event
        creation and handling is performed internally by the SDK library.
        Like the SetDriverEvent functionality it will use less processor
        resources than continuously polling with the GetStatus function. If
        you wish to restart the calling thread without waiting for an
        Acquisition event, call the function CancelWait.
        An Acquisition Event occurs each time a new image is acquired during an
        Accumulation, Kinetic Series or Run-Till-Abort acquisition or at the
        end of a Single Scan Acquisition. If a second event occurs before the
        first one has been acknowledged, the first one will be ignored. Care
        should be taken in this case, as you may have to use CancelWait to
        exit the function.

        Parameters
            NONE

        Returns (python):
            ERROR_CODE[error]
        """
        self._call(self.dll.WaitForAcquisition)

    def SetDriverEvent(self, handle: Optional["PyHANDLE"]) -> None:
        """
        This function passes a Win32 Event handle to the SDK via which the the
        user software can be informed that something has occurred. For example
        the SDK can “set” the event when an acquisition has completed thus
        relieving the user code of having to continually pole to check on the
        status of the acquisition.

        The event will be “set” under the follow conditions:
        1) Acquisition completed or aborted.
        2) As each scan during an acquisition is completed.
        3) Temperature as stabilized, drifted from stabilization or could not
        be reached.

        When an event is triggered the user software can then use other SDK
        functions to determine what actually happened. Condition 1 and 2 can
        be tested via GetStatus function, while condition 3 checked via
        GetTemperature function. You must reset the event after it has been
        handled in order to receive additional triggers. Before deleting the
        event you must call SetDriverEvent with NULL as the parameter.
        """
        self._call(self.dll.SetDriverEvent, handle)

    def GetNumberNewImages(self):
        """
        This function will return information on the number of new images
        (i.e. images which have not   yet  been  retrieved)  in  the
        circular  buffer.  This  information  can  be  used  with
        GetImagesto retrieve a series of the latest images. If any
        images are overwritten in the circular  buffer  they  can  no
        longer  be  retrieved  and  the  information  returned  will
        treat overwritten images as having been retrieved.

        Parameters
            long* first: returns the index of the first available image in the circularbuffer.
            long* last: returns the index of the last available image in the circular buffer.

        Returns (python):
            ERROR_CODE[error]
            first: value of the first new image
            last:  value of the last new image
        """
        first = ct.c_long()
        last = ct.c_long()
        error = self.dll.GetNumberNewImages(ct.byref(first), ct.byref(last))
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error], first.value, last.value

    def GetAcquiredData(self, size: int) -> "NDArray[(Any,), np.int32]":
        """This function will return the data from the last acquisition.

        The data are returned as long integers (32-bit signed integers).
        The “array” must be large enough to hold the complete data set.
        """
        arr = np.empty(size, dtype=np.int32)
        # type information setup on dll initialisation to make this work
        # Check `_add_typing` function for more details
        self._call(self.dll.GetAcquiredData, arr, size)
        return arr

    def GetAcquiredData16(self, size: int) -> "NDArray[(Any,), np.uint16]":
        """
        16-bit version of the GetAcquiredData function. The “array” must
        be large enough to hold the complete data set.

        Parameters
            WORD* arr: pointer to data storage allocated by the user.
            long size: total number of pixels

        Returns (python):
            ERROR_CODE[error]
        """
        arr = np.empty(size, dtype=np.uint16)
        # type information setup on dll initialisation to make this work
        # Check `_add_typing` function for more details
        self._call(self.dll.GetAcquiredData16, arr, size)
        return arr

    def SetExposureTime(self, time: float) -> None:
        """
        This function will set the exposure time to the nearest valid
        value not less than the given value. The actual exposure time
        used is obtained by GetAcquisitionTimings.

        Parameters
            float time: the exposure time in seconds.
        """
        self._call(self.dll.SetExposureTime, ct.c_float(time))

    def GetAcquisitionTimings(self) -> AcquisitionTimings:
        """
        This function will return the current “valid” acquisition timing information.
        This function should be used after all the acquisitions settings have been set,
        e.g. SetExposureTime, SetKineticCycleTime and SetReadMode etc. The values returned
        are the actual times used in subsequent acquisitions.

        This function is required as it is possible to set the exposure time to 20ms,
        accumulate cycle time to 30ms and then set the readout mode to full image.
        As it can take 250ms to read out an image it is not possible to have a cycle time of 30ms.

        Parameters
            float* exposure: valid exposure time in seconds -> exposure
            float* accumulate: valid accumulate cycle time in seconds -> accumulate
            float* kinetic: valid kinetic cycle time in seconds -> kinetic

        Returns (python):
            ERROR_CODE[error]
        """
        exposure = ct.c_float()
        accumulate = ct.c_float()
        kinetic = ct.c_float()
        self._call(
            self.dll.GetAcquisitionTimings,
            ct.byref(exposure),
            ct.byref(accumulate),
            ct.byref(kinetic),
        )
        return AcquisitionTimings(
            exposure=exposure.value, accumulate=accumulate.value, kinetic=kinetic.value
        )

    def SetCoolerMode(self, mode):
        """
        This  function  determines  whether  the  cooler  is  switched
        off  when  the  camera  is  shut down.

        Parameters
            int mode:
                1 Temperature is maintained on ShutDown
                0 Returns to ambient temperature on ShutDown

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetCoolerMode(mode)
        #self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.coolerMode = mode
        return self.__ERROR_CODE[error]

    def SetFanMode(self, mode: FanMode) -> None:
        """
        Allows the user to control the mode of the camera fan. If the system is cooled,
        the fan should only be turned off for short periods of time. During this time the
        body of the camera will warm up which could compromise cooling capabilities.
        If the camera body reaches too high a temperature, depends on camera, the buzzer
        will sound. If this happens, turn off the external power supply and
        allow the system to stabilize before continuing.

        Parameters
            int mode:
                fan on full (0)
                fan on low (1)
                fan off (2)

        Returns (python):
            ERROR_CODE[error]
        """
        self._call(self.dll.SetFanMode, ct.c_int(mode))

    def SetImageRotate(self, iRotate):
        """
        This function will cause data output from the SDK to be rotated on
        one or both axes. This rotate  is  not  done  in  the  camera,
        it  occurs  after  the  data  is  retrieved  and  will increase
        processing  overhead.  If  the  rotation  could  be  implemented
        by  the  user  more  efficiently then use of this function is not
        recomended. E.g writing to file or displaying on screen.

        Parameters
            int iRotate: Rotation setting
                0 No rotation
                1 Rotate 90 degrees clockwise
                2 Rotate 90 degrees anti-clockwise

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetImageRotate(iRotate)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error]

    def SaveAsFITS(self, filename, type):
        """
        This  function  saves  the  last  acquisition  in  the  FITS
        (Flexible  Image  Transport  System) Data Format (*.fits)
        endorsed by NASA.

        Parameters
            char* szFileTitle: thefilename to save too.
            int typ:Valid values:
                0 Unsigned 16
                1 Unsigned 32
                2 Signed 16
                3 Signed 32
                4 Float

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SaveAsFITS(filename, type)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error]

    def CoolerON(self) -> None:
        """
        Switches ON the cooling. On some systems the rate of temperature
        change is controlled until the temperature is within 3degC of the
        set  value. Control  is returned immediately to the calling
        application.
        """
        self._call(self.dll.CoolerON)

    def CoolerOFF(self) -> None:
        """
        Switches OFF the cooling. The rate of temperature change is
        controlled in some models until the temperature reaches 0degC.
        Control is returned immediately to the calling application.
        """
        self._call(self.dll.CoolerOFF)

    def IsCoolerOn(self) -> bool:
        """
        This function checks the status of the cooler.

        Returns (python):
            int* iCoolerStatus:
                0 Cooler is OFF.
                1 Cooler is ON.
        """
        iCoolerStatus = ct.c_int()
        self._call(self.dll.IsCoolerOn, ct.byref(iCoolerStatus))
        return bool(iCoolerStatus.value)

    def GetTemperature(self) -> Tuple[int, TemperatureStatus]:
        """

        This  function  returns  the  temperature of  the  detector to
        the nearest degree. It also gives the status of cooling process.
        """
        ctemperature = ct.c_int()
        code = self._call(
            self.dll.GetTemperature,
            ct.byref(ctemperature),
            errors_ok=set(TemperatureStatus),
        )
        return ctemperature.value, TemperatureStatus(code)

    def GetTemperatureRange(self) -> Tuple[int, int]:
        """
        This function returns the valid range of temperatures in
        centigrade to which the detector can be cooled.

        Parameters
            int* mintemp: minimum temperature
            int* maxtemp: maximum temperature

        Returns (python):
            ERROR_CODE[error]
        """
        minTemp = ct.c_int()
        maxTemp = ct.c_int()
        self._call(self.dll.GetTemperatureRange, ct.byref(minTemp), ct.byref(maxTemp))
        return minTemp.value, maxTemp.value

    def GetTemperaturePrecision(self):
        """
        This function returns the number of decimal places to which the
        sensortemperature can be returned.

        Parameters
            int* precision: number of decimal places

        Returns (python):
            ERROR_CODE[error]
        """

        percisionTemp = ct.c_int()
        error = self.dll.GetTemperaturePrecision(percisionTemp)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        # accept temp errors
        if error in [20034, 20035, 20036, 20037, 20038, 20039, 20040]:
            self.tempePrecision = percisionTemp.value
        return self.__ERROR_CODE[error]

    def SetTemperature(self, temperature: int) -> None:
        """
        This function will set the desired temperature of the detector.
        To turn the cooling ON and OFF use the CoolerON and CoolerOFF
        function respectively.

        Parameters
            int temperature: the temperature in Centigrade.
            Valid range is given by GetTemperatureRange
        """
        self._call(self.dll.SetTemperature, ct.c_int(temperature))

    def GetEMCCDGain(self) -> int:
        """
        Returns  the  current  gain  setting.  The  meaning  of  the
        value  returned  depends  on  the  EM Gain mode.
        """
        gain = ct.c_int()
        self._call(self.dll.GetEMCCDGain, ct.byref(gain))
        return gain.value

    def SetEMGainMode(self, gainMode: EMGainMode) -> None:
        """
        Set the EM Gain mode to one of the following possible settings.

        Mode
            0 The EM Gain is controlled by DAC settings in the range 0-255.
                Default mode.
            1 The EM Gain is controlled by DAC settings in the range 0-4095.
            2 Linear mode.
            3 Real EM gain

        To access higher gain values (if available)
        it is necessary to enable advanced EM gain using SetEMAdvanced
        """
        self._call(self.dll.SetEMGainMode, ct.c_int(gainMode))

    def SetEMCCDGain(self, gain: int) -> None:
        """
        Allows the user to change the gain value. The valid range for the gain
        depends on what gain mode the camera is operating in. See SetEMGainMode
        to set the mode and GetEMGainRange to get the valid range to work with.
        To access higher gain values (>x300) see SetEMAdvanced.
        Parameters
            int gain: amount of gain applied.
        """
        self._call(self.dll.SetEMCCDGain, ct.c_int(gain))

    def SetEMAdvanced(self, gainAdvanced: bool) -> None:
        """
        This function turns on and off access to higher EM gain levels
        within the SDK. Typically, optimal signal to noise ratio and dynamic
        range is achieved between x1 to x300 EM Gain.
        Higher gains of > x300 are recommended for single photon counting only.
        Before using higher levels, you should ensure that light levels
        do not exceed the regime of tens of photons per pixel, otherwise
        accelerated ageing of the sensor can occur.

        Parameters
            int state: Enables/Disables access to higher EM gain levels
                1 – Enable access
                0 – Disable access
        """
        self._call(self.dll.SetEMAdvanced, ct.c_int(gainAdvanced))

    def GetEMGainRange(self) -> Tuple[int, int]:
        """
        Returns  the  minimum  and  maximum  values  of  the  current
        selected  EM  Gain  mode  and temperature of the sensor.

        Returns (python):
            ERROR_CODE[error]
        """
        low = ct.c_int()
        high = ct.c_int()
        self._call(self.dll.GetEMGainRange, ct.byref(low), ct.byref(high))
        return low.value, high.value

    def isTriggerModeAvailable(self, trigger_mode: TriggerMode) -> bool:
        """This function checks if the hardware and current settings permit the
        use of the specified trigger mode
        """
        code = self._call(
            self.dll.isTriggerModeAvailable,
            ct.c_int(trigger_mode),
            errors_ok={ErrorCode.DRV_INVALID_MODE},
        )
        return code == ErrorCode.DRV_SUCCESS

    def GetNumberADChannels(self) -> int:
        """
        As  your Andor  SDK system  may  be  capable  of  operating  with
        more  than  one  A-D converter, this function will tell you the
        number available.

        Parameters
            int* channels: number of allowed channels

        Returns (python):
            ERROR_CODE[error]
        """
        noADChannels = ct.c_int()
        self._call(self.dll.GetNumberADChannels, ct.byref(noADChannels))
        return noADChannels.value

    def GetBitDepth(self, channel: int) -> int:
        """
        This  function  will  retrieve  the size  in  bits  of  the
        dynamic  range  for  any  available  AD channel.

        Parameters
            int channel: the AD channel.
            int* depth: dynamic range in bits.

        Returns (python):
            ERROR_CODE[error]
        """
        bitDepth = ct.c_int()
        self._call(self.dll.GetBitDepth, ct.c_int(channel), ct.byref(bitDepth))
        return bitDepth.value

    def SetADChannel(self, index: int) -> None:
        """
        This function will set the AD channel to one of the possible A-Ds
        of thesystem. This AD channel will be used for all subsequent
        operations performed by the system.

        Parameters
            int index: the channel to be used
                Valid values: 0 to GetNumberADChannels-1
        """
        self._call(self.dll.SetADChannel, ct.c_int(index))

    def SetOutputAmplifier(self, index):
        """
        Some  EMCCD systems  have  the  capability  to  use  a  second
        output  amplifier.  This function will set the type of output
        amplifier to be used when reading data from the head for these
        systems.

        Parameters
            int typ: the type of output amplifier.
                0 Standard EMCCD gain register (default)/Conventional(clara).
                1 Conventional CCD register/Extended NIR mode(clara).

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetOutputAmplifier(index)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.outamp = index
        return self.__ERROR_CODE[error]

    def GetNumberHSSpeeds(self, channel: int, typ: OutputAmplificationMode) -> int:
        """
        As your Andor SDK system is capable of operating at more than one
        horizontal shift speed this function will return the actual number
        of speeds available.

        Parameters
            int channel: the AD channel.
            int typ: output amplification.
                Valid values:
                    0 electron multiplication.
                    1 conventional.
            int* speeds: number of allowed horizontal speeds

        Returns (python):
            ERROR_CODE[error]
        """
        noHSSpeeds = ct.c_int()
        self._call(
            self.dll.GetNumberHSSpeeds,
            ct.c_int(channel),
            ct.c_int(typ),
            ct.byref(noHSSpeeds),
        )
        return noHSSpeeds.value

    def GetHSSpeed(
        self, channel: int, typ: OutputAmplificationMode, index: int
    ) -> float:
        """
        As your Andor system is capable of operating at more than one
        horizontal shift speed this function will return the actual speeds
        available. The value returned is in MHz.

        Parameters
            int channel: the AD channel.
            int typ: output amplification.
                Valid values:
                    0 electron multiplication/Conventional(clara).
                    1 conventional/Extended NIR Mode(clara).
            int index: speed required
                Valid values:
                    0 to NumberSpeeds-1 where NumberSpeeds
                    is value returned in first parameter after a
                    call to GetNumberHSSpeeds().

        Returns (python):
            float speed: speed in in MHz
        """
        HSSpeed = ct.c_float()

        self._call(
            self.dll.GetHSSpeed,
            ct.c_int(channel),
            ct.c_int(typ),
            ct.c_int(index),
            ct.byref(HSSpeed),
        )
        return HSSpeed.value

    def SetHSSpeed(self, typ: OutputAmplificationMode, index: int) -> None:
        """
        This function will set the speed at which the pixels are shifted into
        the output node during the readout phase of an acquisition. Typically
        your camera will be capable of operating at several horizontal shift
        speeds. To get the actual speed that an index corresponds to use the
        GetHSSpeed function.

        Parameters
            int typ: output amplification.
                Valid values:   0 electron multiplication/Conventional(clara).
                                1 conventional/Extended NIR mode(clara).
            int index: the horizontal speed to be used
                Valid values:   0 to GetNumberHSSpeeds()-1
        """
        self._call(self.dll.SetHSSpeed, ct.c_int(typ), ct.c_int(index))

    def GetNumberVSSpeeds(self) -> int:
        """
        As your Andor system may be capable of operating at more than one
        vertical shift speed this function will return the actual number
        of speeds available.
        """
        noVSSpeeds = ct.c_int()
        self._call(self.dll.GetNumberVSSpeeds, ct.byref(noVSSpeeds))
        return noVSSpeeds.value

    def GetVSSpeed(self, index: int) -> float:
        """
        As your Andor  SDK system may be capable of operating at more than
        one vertical shift speed  this  function  will  return  the
        actual  speeds  available.  The  value  returned  is  in
        microseconds.

        Parameters
            int index: speed required
                Valid values
                    0 to GetNumberVSSpeeds()-1

        Returns (python):
            float* speed: speed in microseconds per pixel shift
        """
        VSSpeed = ct.c_float()

        self._call(self.dll.GetVSSpeed, ct.c_int(index), ct.byref(VSSpeed))
        return VSSpeed.value

    def SetVSSpeed(self, index: int):
        """
        This function will set the vertical speed to be used for
        subsequent acquisitions

        Parameters
            int index: index into the vertical speed table
                Valid values
                    0 to GetNumberVSSpeeds-1
        """
        self._call(self.dll.SetVSSpeed, ct.c_int(index))

    def GetFastestRecommendedVSSpeed(self) -> Tuple[int, float]:
        """
        As  your  Andor  SDK  system  may  be  capable  of  operating  at
        more  than  one  vertical  shift speed  this  function  will
        return  the  fastest  recommended  speed  available.    The  very
        high readout  speeds,  may  require  an  increase  in  the
        amplitude  of  the  Vertical  Clock  Voltage using SetVSAmplitude.
        This function returns the fastest speed which does not require the
        Vertical  Clock  Voltage  to  be  adjusted.    The  values
        returned  are  the  vertical  shift  speed index and the actual
        speed in microseconds per pixel shift.

        Returns (python):
            Int index: index of the fastest recommended vertical shift speed
            float speed: speed in microseconds per pixel shift.
        """
        VSSpeed = ct.c_float()
        VSSpeedIndex = ct.c_int()
        self._call(
            self.dll.GetFastestRecommendedVSSpeed,
            ct.byref(VSSpeedIndex),
            ct.byref(VSSpeed),
        )
        return VSSpeedIndex.value, VSSpeed.value

    def SetVSAmplitude(self, state):
        """
        If you choose a high readout speed (a low readout time), then you
        should also consider increasing the amplitude of the Vertical
        Clock Voltage. There are five levels of amplitude available for
        you to choose from:
            * 0 = Normal
            * 1 = +1
            * 2 = +2
            * 3 = +3
            * 4 = +4
        Exercise caution when increasing the amplitude of the vertical
        clock voltage, since higher clocking voltages may result in
        increased clock-induced charge (noise) in your signal. In general,
        only  the very  highest  vertical  clocking  speeds  are  likely
        to  benefit  from  an increased vertical clock voltage amplitude.

        Parameters
            int state: desired Vertical Clock Voltage Amplitude
                Valid values:   0 -Normal
                                1->4 –Increasing Clock voltage Amplitude

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetVSAmplitude(state)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.stateVSAmplitude = state
        return self.__ERROR_CODE[error]

    def GetNumberVSAmplitudes(self):
        """
        This function will normally return the number of vertical clock
        voltage amplitudes that the camera has.

        Parameters:
            int number: Number of VSAmplitudes
        """

        noAmps = ct.c_int()
        error = self.dll.GetNumberVSAmplitudes(ct.byref(noAmps))
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.noVSAmplitudes = noAmps.value
            return noAmps.value
        return self.__ERROR_CODE[error]

    def GetNumberPreAmpGains(self) -> int:
        """
        Available  in  some  systems  are  a  number  of  pre  amp  gains
        that  can  be  applied  to  the data as it is read out. This
        function gets the number of these pre amp gains available. The
        functions GetPreAmpGain and SetPreAmpGain can  be  used  to  specify
        which  of these gains is to be used.

        Parameters
            int* noGains: number of allowed pre amp gain

        Returns (python):
            ERROR_CODE[error]
        """
        noGains = ct.c_int()
        self._call(self.dll.GetNumberPreAmpGains, ct.byref(noGains))
        return noGains.value

    def GetPreAmpGain(self, index: int) -> float:
        """
        For those systems that provide a number of pre amp gains to apply
        to the data as it is read out;  this  function  retrieves  the
        amount  of  gain  that  is  stored  for  a  particular  index.
        The number  of  gains  available  can  be  obtained  by  calling
        the GetNumberPreAmpGainsfunction and a specific Gain can be
        selected using the function SetPreAmpGain.

        Parameters
            int index: gain index
            Valid values:
                0 to GetNumberPreAmpGains()-1
                float* gain: gain factor for this index.

        Returns (python):
            ERROR_CODE[error]
        """
        gain = ct.c_float()
        self._call(self.dll.GetPreAmpGain, ct.c_int(index), ct.byref(gain))
        return gain.value

    def SetPreAmpGain(self, index):
        """
        This function will set the pre amp gain to be used for subsequent
        acquisitions. The actual gain  factor  that  will  be  applied
        can  be  found  through  a  call  to  the GetPreAmpGainfunction.
        The number of Pre Amp Gains available is found by calling the
        GetNumberPreAmpGains function.

        Parameters
            int index: index pre amp gain table
                Valid values
                    0 to GetNumberPreAmpGains-1

        Returns (python):
            self.__ERROR_CODE[error]
        """
        self._call(self.dll.SetPreAmpGain, ct.c_int(index))

    def SetTriggerMode(self, mode: TriggerMode) -> None:
        """
        This function will set the trigger mode that the camera will
        operate in.

        Parameters
            int mode: trigger mode
                Valid values:
                    0 Internal
                    1 External
                    6 External Start
                    7 External Exposure (Bulb)
                    9 External FVB EM (only valid for EM Newton models in FVB mode)
                    10 Software Trigger
                    12 External Charge Shifting
        """
        self._call(self.dll.SetTriggerMode, ct.c_int(mode))

    def GetStatus(self) -> Status:
        """
        This function will return the current status of the Andor SDK
        system. This function should be called before an acquisition is
        started to ensure that it is IDLE and during an acquisition to
        monitor the process.

        Parameters
            int* status: current status

        Returns (python):
            ERROR_CODE[status]
        """
        status = ct.c_int()
        self._call(self.dll.GetStatus, ct.byref(status))
        return Status(status.value)

    def SetFastExtTrigger(self, mode: bool) -> None:
        """
        This function will enable fast external triggering. When fast external
        triggering is enabled the system will NOT wait until a “Keep Clean”
        cycle has been completed before accepting the next trigger. This
        setting will only have an effect if the trigger mode has been set to
        External via SetTriggerMode.

        Parameters
            int mode:
                0 Disabled
                1 Enabled
        """
        self._call(self.dll.SetFastExtTrigger, ct.c_int(mode))

    def SetTriggerInvert(self, mode):
        """
        This function will set whether an acquisition will be triggered
        on a rising or falling edge external trigger.

        Parameters
            int mode: trigger mode
                0 Rising Edge
                1 Falling Edge

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetTriggerInvert(mode)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.triggerInvert = mode
        return self.__ERROR_CODE[error]

    def GetCapabilities(self):
        caps = CtCapabilities()
        caps.size = ct.sizeof(caps)
        self._call(self.dll.GetCapabilities, ct.byref(caps))
        return caps

    def GetAcquisitionProgress(self):
        """
        This  function  will  return  information  on  the  progress  of
        the  current  acquisition.  It  can  be called at any time but is
        best used in conjunction with SetDriverEvent. The values returned
        show the number of completed scans in the current acquisition.
        If 0 is returned for both accum and series then either:
            * No acquisition is currently running
            * The acquisition has just completed
            * The very first scan of an acquisition has just started and
            not yet completed
        GetStatus can be used to confirm if the first scan has just
        started, returning DRV_ACQUIRING, otherwise it will return
        DRV_IDLE.
        For example, if accum=2 and series=3 then the acquisition has
        completed 3 in the series and 2 accumulations in the 4 scan of the
        series.

        Parameters
            long*  acc:  returns  the  number  of  accumulations completed  in the current kinetic scan.
            long* series: return the number of kinetic scans completed

        Returns (python):
            ERROR_CODE[error]
        """
        acc = ct.c_long()
        series = ct.c_long()
        error = self.dll.GetAcquisitionProgress(ct.byref(acc), ct.byref(series))
        if self.__ERROR_CODE[error] == "DRV_SUCCESS":
            return series.value
        else:
            return None

    def GetReadOutTime(self):
        """
        This function will return the time to readout data from a sensor.
        This function should be used after all the acquisitions settings have
        been set, e.g. SetExposureTime, SetKineticCycleTime and SetReadMode etc.
        The value returned is the actual times used in subsequent acquisitions.

        Parameters
            float* ReadoutTime: valid readout time in seconds

        Returns (python):
            ERROR_CODE[error]
        """
        roTime = ct.c_float()
        error = self.dll.GetReadOutTime(ct.byref(roTime))
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        if error == 20002:
            self.roTime = roTime.value
        return self.__ERROR_CODE[error]

    def SetFrameTransferMode(self, frameTransfer: bool):
        """
        This function will set whether an acquisition will
        readout in Frame Transfer Mode. If the acquisition
        mode is Single Scan or Fast Kinetics this call will
        have no affect.

        Parameters
            int mode:
                 0 OFF
                 1 ON

        Returns (python):
            ERROR_CODE[error]
        """
        self._call(self.dll.SetFrameTransferMode, ct.c_int(frameTransfer))

    def SetShutterEx(self, typ, mode, closingtime, openingtime, extmode):
        """
        This function expands the control offered by SetShutterto allow an
        external shutter and internal shutter to be controlled
        independently (only available on some cameras –please consult your
        Camera User Guide). The typparameter allows the user to control
        the TTL signal output to an external shutter. The opening and
        closing times specify the length of time required to open and
        close the shutter (this information is required for calculating
        acquisition timings –see SHUTTER TRANSFER TIME).The mode and
        extmode parameters control the behaviour of the internal and
        external shutters. To have an external shutter open and close
        automatically in an experiment, set the mode parameter to “Open”
        and set the extmode parameter to “Auto”. To have an internal
        shutter open and close automatically in an experiment, set the
        extmode parameter to “Open” and set the mode parameter to
        “Auto”. To not use any shutter in the experiment, set both shutter
        modes to permanently open.

        Parameters
            Int typ:
                0 Output TTL low signal to open shutter
                1 Output TTL high signal to open shutter
            int mode:
                0 Fully Auto
                1 Permanently Open
                2 Permanently Closed
                4 Open for FVBseries
                5 Open for anyseries
            int closingtime: time shutter takes to close (milliseconds)
            int openingtime: Time shutter takes to open (milliseconds)
            int extmode:
                0 Fully Auto
                1 Permanently Open
                2 Permanently Closed
                4 Open for FVBseries
                5 Open for anyseries

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetShutterEx(typ, mode, closingtime, openingtime, extmode)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error]

    def SetSpool(self, active, method, path, framebuffersize):
        """
        This function will enable and disable the spooling of acquired
        data to the hard disk or to the RAM.
        With spooling method 0, each scan in the series will be saved to
        a separate file composed of a sequence of 32-bit integers.
        With spooling method 1 the type of data in the output files
        depends on what type of acquisition is takingplace (see below).
        Spooling method 2 writes out the data to file as 16-bit integers.
        Spooling method 3 creates a directory structure for storing images
        where multiple images may appear in each file within the directory
        structure and the files may be spread across multiple directories.
        Like method 1 the data type of the image pixels depends on whether
        accumulate mode is being used. Method  4  Creates  a  RAM  disk
        for  storing  images  so  you  should  ensure  that  there  is
        enough free RAM to store the full acquisition. Methods 5, 6 and 7
        can be used to directly spool out to a particular file type,
        either FITS, SIF or TIFF respectively. In the case of FITS and
        TIFF the data will be written out as 16-bit values.
        Method  8  is  similar  to  method  3,  however  the  data  is
        first compressed  before  writing  to disk.  In  some
        circumstances  this  may  improve  the  maximum  rate  of  writing
        images  to disk,  however  as  the  compression  can  be  very CPU
        intensive  this  option  may  not  be suitable on slower
        processors.The  data  is  stored  in  row  order  starting  with
        the  row  nearest  the  readout  register.  With the exception of
        methods 5, 6 and 7, the data acquired during a spooled acquisition
        can be retrieved through the normal functions. This is a change to
        previous versions; it is no longer necessary to load the data from
        disk from your own application.

        Parameters
            int active: Enable/disable spooling
                Valid values:
                    0 Disable spooling.
                    1 Enable spooling.
            int method: Indicates the format of the files written to disk
                Valid values:
                    0 Files contain sequence of 32-bit integers
                    1 Format of data in files depends on whether multiple
                      accumulations are being taken for each scan. Format
                      will be 32-bit integer if data is being accumulated
                      each scan; otherwise the format will be 16-bit
                      integer.
                    2 Files contain sequence of 16-bit integers.
                    3 Multiple directory structure with multiple images
                      per file and multiple filesper directory.
                    4 Spool to RAM disk.
                    5 Spool to 16-bit Fits File
                    6 Spool to Andor Sif format
                    7 Spool to 16-bit Tiff File.
                    8 Similar to method 3 but with data compression.
            char* path: String containing the filename stem. May also
                        contain the path to thedirectory into which the
                        files are to be stored.
            int framebuffersize: This sets the size of an internal
                circular buffer used as temporarystorage. The value is
                the total number images the buffer can hold, not the size
                in bytes. Typical value would be 10. This value would be
                increased in situations where the computer is not able to
                spool the data to disk at therequired rate.

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetSpool(active, method, ct.c_char_p(path), framebuffersize)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error]

    def SetSingleTrack(self, centre, height):
        """
        This  function  will  set  the  single  track  parameters.  The
        parameters  are  validated  in  the following order: centre row
        and then track height.

        Parameters
            int centre: centre row of track
                Valid range
                    1 to number of vertical pixels.
            int height: height of track
                Valid range
                    > 1 (maximum value depends on centre row and number of
                        vertical pixels).

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.dll.SetSingleTrack(centre, height)
        self.verbose(self.__ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.__ERROR_CODE[error]

    def SetDemoReady(self):
        """
        Take a singele image for demo purposes

        Returns (python):
            ERROR_CODE[error]
        """
        error = self.SetSingleScan()
        error = self.SetTriggerMode(0)
        error = self.SetShutter(1, 0, 30, 30)
        error = self.SetExposureTime(0.01)
        return error

    def SetBinning(self, binningmode):
        """
        Set to bin the images in hardware inside the camera


        Returns (python):
            self.__ERROR_CODE[error]
        """
        if binningmode == 1:
            self.SetImage(1, 1, 1, self.width, 1, self.height)
        elif binningmode == 2:
            self.SetImage(2, 2, 1, self.width, 1, self.height)
        elif binningmode == 4:
            self.SetImage(4, 4, 1, self.width, 1, self.height)
        else:
            self.verbose("Binning mode not found")

    ###
    ### functions for the gui
    ###

    def acquire_image(self, externalTrigger):
        """
        Acquire an image in the current camera state

        Parameters (python)
            externalTrigger: whether to trigger immediately or externally
                Valid values:
                    0 Internal
                    1 External
                    6 External Start
                    7 External Exposure (Bulb)
                    9 External FVB EM (only valid for EM Newton models in FVB mode)
                    10 Software Trigger
                    12 External Charge Shifting
        """

        if externalTrigger == self.triggerMode:
            pass
        elif externalTrigger in [0, 1, 6, 7, 9, 10, 12]:
            self.SetTriggerMode(externalTrigger)
        else:
            # TODO: how to handle wrong trigger setting?
            warnings.warn("Invalid triggermode")
        self.GetAcquisitionTimings()
        self.StartAcquisition()
        self.WaitForAcquisition()
        data1 = []
        while self.GetAcquiredData16(data1) == "DRV_ACQUIRING":
            data1 = []
            time.sleep(100e-3)
        arr = np.array(data1).reshape(self.scans, self.height, self.width)

        return arr

    def prepare_take1x4(self, exposureTime=None):
        """
        setup the camera to take 4images with 1 trigger
        """
        if exposureTime == None:
            exposureTime = 0.032  # 32ms exposure
        self.SetAcquisitionMode(3)  # 3: Kinetics
        self.SetFrameTransferMode(1)
        self.SetExposureTime(exposureTime)  # expose for 32ms
        self.SetAccumulationCycleTime(1)  # in seconds. Is it useful?
        self.SetNumberAccumulations(1)  # how many repetitions
        self.SetNumberKinetics(4)  # Number of images taken
        self.SetKineticCycleTime(0.003257)  # in seconds.
        self.SetTriggerMode(6)  # 0:Int, 1:Ext, 6:Ext Start, 7:Ext Start Stop
        self.SetFastExtTrigger(1)
        self.SetHSSpeed(self.outamp, 0)  # 2nd parameter: 0: 27 MHz, 1: 13 MHz, 2: 5 MHz
        self.SetVSSpeed(0)  # 0: 0.56 us, 1: 1.02 us, 2: 1.92 us, 3: 3.72 us

        self.SetEMGainMode(
            1
        )  # 1 The EM Gain is controlled by DAC settings in the range 0-4095.
        self.SetEMCCDGain(0)  # 2000 for darkfield

        self.imaging_mode = "Take1x4"

        return None

    def prepare_take3(self, exposureTime=None):
        """
        setup the camera to take 3 images with 3 triggers
        """
        if exposureTime == None:
            exposureTime = 0.032  # 32ms exposure
        self.SetExposureTime(exposureTime)  # set exposure time
        if self.triggerMode != 1:
            self.SetTriggerMode(1)  # trigger every imag
        if self.acquisitionMode != 3:
            self.SetAcquisitionMode(3)  # 3: KineticSeries
        if self.frameTransfer != 0:
            self.SetFrameTransferMode(0)  # disable frame transfer
        if self.readMode != 4:
            self.SetReadMode(4)  # read the full image
        if self.numberAccumulations != 1:
            self.SetNumberAccumulations(1)  # take only one image
        if self.scans != 3:
            self.SetNumberKinetics(3)  # take three images

        self.imaging_mode = "Take3"

        return None

    def prepare_single(self, exposureTime=None):

        if exposureTime == None:
            exposureTime = 0.032  # 32ms exposure
        self.SetExposureTime(exposureTime)  # set exposure time

        self.SetAcquisitionMode(1)  # set to single image
        self.SetFrameTransferMode(0)  # disable frame transfer
        self.SetReadMode(4)  # read the full image
        self.SetNumberAccumulations(1)  # take only one image
        self.SetNumberKinetics(1)  # take only one image
        self.SetImage(1, 1, 1, self.width, 1, self.height)

        return None
