import dataclasses
from pico_control_client import PicoControlClient
from config import config_dict
from qcontrol3.driver.camera.andor2.andor import *
from qcontrol3.driver.camera.andor2 import camera
import matplotlib.pyplot as plt
import numpy as np
import time
import pickle

# SET UP CAMERA
andor = camera.AndorCamera(1750)

print(andor.device_info)

andor.number_accumulations = 1
andor.driver.SetShutter(
    TTLMode.ActiveHigh, ShutterMode.Open, closingtime=10, openingtime=10
)

andor.exposure_time = 500e-3  # Quantity
andor.acquisition_mode = AcquisitionMode.Kinetics
andor.read_mode = ReadMode.Image
#andor.em_gain=220
andor.frame_transfer_mode = True
andor.number_kinetics = 1  # FRAME_COUNT, int
andor.driver.SetImage(**dataclasses.asdict(andor.image_parameters))  # int

# SET UP PICOMOTORS
pcc = PicoControlClient("http://picoswitch.cslab:8080")
pcc.set_speed(500)
picomotors_z = config_dict["modes"][2][1]


def take_single_image() -> np.array:
    andor.driver.StartAcquisition()
    while True:
        try:
            data = andor.get_acquired_data()
            break
        except AndorException as e:
            if e.error_code == ErrorCode.DRV_ACQUIRING:
                continue
            raise
    return data[0]


def move_z(direction, n_pico_steps):
    for picomotor in picomotors_z:
        print(picomotor)
        current_module, current_channel = picomotor
        response = pcc.move((current_module, current_channel),
                            direction * n_pico_steps * config_dict["picomotors"][(current_module, current_channel)][
                                "direction"])
        time.sleep(1.2)


### EXPERIMENT ###


plt.ion()
fig = plt.figure(figsize=(15, 15))

wstream = open("V:\\Caesium\\Projects\\High-resolution imaging\\datanew4.pkl", "wb")

imgs = []
for i in range(0, 40):
    fig.clear()
    print(i)
    img = take_single_image()
    plt.imshow(np.log(img))
    fig.canvas.draw()
    plt.show()
    time.sleep(0.5)
    imgs.append(img)
    move_z(-1, 10)
    time.sleep(1)

for i in range(0, 40):
    fig.clear()
    print("r",i)
    img = take_single_image()
    plt.imshow(np.log(img))
    fig.canvas.draw()
    plt.show()
    time.sleep(0.5)
    imgs.append(img)
    move_z(1, 10)
    time.sleep(1)

pickle.dump(imgs, wstream)


