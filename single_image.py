import dataclasses
from qcontrol3.driver.camera.andor2.andor import *
from qcontrol3.driver.camera.andor2 import camera
from qcontrol3.driver.camera.andor2.camera import ImageParameters
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
import time
import pickle
import scipy.optimize as opt
import matplotlib.patches as patches

# SET UP CAMERA
andor = camera.AndorCamera(1750)

print(andor.device_info)

andor.number_accumulations = 1
andor.driver.SetShutter(
    TTLMode.ActiveHigh, ShutterMode.Open, closingtime=10, openingtime=10
)

andor.exposure_time = 500e-3  # Quantity
andor.acquisition_mode = AcquisitionMode.Kinetics
#andor.em_gain=110
andor.read_mode = ReadMode.Image
andor.frame_transfer_mode = False
andor.number_kinetics = 1  # FRAME_COUNT, int
andor.driver.SetImage(**dataclasses.asdict(andor.image_parameters))  # int
#andor.image_parameters = ImageParameters(
#            hbin=1,
#            vbin=1,
#            hstart=1,
#            vstart=1,
#            hend=800,
#            vend=800,
#        )

cal=8000/58
gauss_factor = 2.865

def take_single_image() -> np.array:
    andor.driver.StartAcquisition()
    while True:
        try:
            data = andor.get_acquired_data()
            break
        except AndorException as e:
            if e.error_code == ErrorCode.DRV_ACQUIRING:
                continue
            raise
    return data[0]


def twoD_Gaussian(XY, amplitude, xo, yo, sigma_x, sigma_y, offset):
    theta = 0
    x, y = XY
    xo = float(xo)
    yo = float(yo)
    a = (np.cos(theta) ** 2) / (2 * sigma_x ** 2)
    c = (np.cos(theta) ** 2) / (2 * sigma_y ** 2)
    g = offset + amplitude * np.exp(- (a * ((x - xo) ** 2)
                                       + c * ((y - yo) ** 2)))
    return g.ravel()


def fit_2d_gaussian(data, initial_guess, maxfev=20000):
    data = np.asarray(data)
    sx, sy = data.shape
    x = np.arange(0, sy, 1)
    y = np.arange(0, sx, 1)
    x, y = np.meshgrid(x, y)
    crop = data

    popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), crop.ravel(), p0=initial_guess, maxfev=maxfev)
    ampl, cx, cy, sigmax, sigmay, offset = popt
    data_fitted = twoD_Gaussian((x, y), *popt)
    crop = data

    return popt


### EXPERIMENT ###


popt_hist = []

plt.ion()
fig = plt.figure(constrained_layout=True, figsize=(15, 15))
while True:
    print("newimg")
    fig.clear()
    gs = fig.add_gridspec(3,3)

    #main image
    fig_ax1 = fig.add_subplot(gs[0:2,0:2])
    img = take_single_image()
    fig_ax1.imshow(img+1)

    #cropping
    crop = img[475:525, 475:525]
    rect = patches.Rectangle((475, 475),50,50,0, facecolor="none", edgecolor="red")
    fig_ax1.add_patch(rect)

    #crop image
    fig_ax2 = fig.add_subplot(gs[-1,0])
    fig_ax2.imshow(crop)
    # plt.contour(x, y, data_fitted.reshape(sx,sy), 8, colors='r')
    fig_ax2.set_xlabel("x (px)")
    fig_ax2.set_ylabel("y (px)")

    # fitting
    max_pos = np.asarray(np.where(crop == np.max(crop)))[:,0].flatten()[::-1]
    initial_guess=(200, *max_pos,10,10,1500)
    try:
        popt= fit_2d_gaussian(crop,initial_guess,maxfev=5000)
    except:
        popt = (1,1,1,1,1,1)

    ampl, cx, cy, sigmax, sigmay, offset = popt
    popt_hist.append(popt)


    sx, sy = crop.shape
    x = np.arange(0, sy, 1)
    y = np.arange(0, sx, 1)
    x, y = np.meshgrid(x, y)
    data_fitted = twoD_Gaussian((x, y), *popt)

    #crop image fit center
    fig_ax2.axvline(cx, color="red")
    fig_ax2.axhline(cy, color="red")


    #fit plot
    fig_ax3 = fig.add_subplot(gs[-1,1])
    try:
        fig_ax3.scatter(np.arange(len(crop[int(cy), :])), crop[int(cy), :])
        fig_ax3.plot(np.arange(len(data_fitted.reshape(sx, sy)[int(cy), :])), data_fitted.reshape(sx, sy)[int(cy), :],color="red")
        fig_ax3.set_xlabel("x (px)")
        fig_ax3.set_ylabel("Int. (ar. u.)")
        fig_ax3.set_title(sigmax*cal*gauss_factor)
    except IndexError:
        pass



    fig_ax4 = fig.add_subplot(gs[-1,-1])
    try:
        fig_ax4.scatter(np.arange(len(crop[:, int(cx)])), crop[:, int(cx)])
        fig_ax4.plot(np.arange(len(data_fitted.reshape(sx, sy)[:, int(cx)])), data_fitted.reshape(sx, sy)[:, int(cx)],color="red")
        fig_ax4.set_xlabel("y (px)")
        fig_ax4.set_ylabel("Int. (ar. u.)")
        fig_ax4.set_title(sigmay*cal*gauss_factor)
    except IndexError:
        pass


    #history plot
    fig_ax5 = fig.add_subplot(gs[1, 2])
    sigmaxs = np.asarray(popt_hist)[:,3]
    sigmays = np.asarray(popt_hist)[:,4]
    fig_ax5.scatter(np.arange(0, len(sigmaxs)), np.abs(sigmaxs))
    fig_ax5.scatter(np.arange(0, len(sigmays)), np.abs(sigmays))
    fig_ax5.set_ylim(0, 10)

    fig_ax6 = fig.add_subplot(gs[0, 2])
    ampls = np.asarray(popt_hist)[:,0]
    fig_ax6.scatter(np.arange(0, len(ampls)), ampls)
    fig_ax6.set_ylim(0, 800)

    #update figure
    fig.canvas.draw()
    fig.canvas.flush_events()

    np.savetxt("histnew.dat",popt_hist)

    # time.sleep(60)

    wstream = open("single_image.pkl", "wb")

    pickle.dump(img, wstream)
